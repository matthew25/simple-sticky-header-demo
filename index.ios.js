/**
 * Created by matthewjamieson on 27/04/15.
 */
'use strict';

var React = require('react-native');
var {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ScrollView
    } = React;

var fantasyfeaturetest = React.createClass({
    render: function () {
        var items = [];
        var headerIndices = [];

        for (var i = 0; i < 500; i++) {
            var isHeader = (i % 10 == 0);
            var style = isHeader ? styles.header : styles.listItem;

            items.push(
                <View style={style}>
                    <Text> {i} </Text>
                </View>
            );

            if (isHeader) {
                headerIndices.push(i);
            }
        }
        return (
                <ScrollView style={styles.scrollView} stickyHeaderIndices={headerIndices}>
                    {items}
                </ScrollView>
        );
    }
});

var styles = StyleSheet.create({
    scrollView: {
        marginTop: 80
    },
    header: {
        flex: 1,
        paddingLeft: 10,
        backgroundColor: '#aaaaaa'
    },
    listItem: {
        flex: 1,
        paddingLeft: 10,
        backgroundColor: '#ffffff'
    }
});

AppRegistry.registerComponent('fantasyfeaturetest', () => fantasyfeaturetest);
